package com.parking.lease.rental;

import com.parking.lease.exception.ParkingSpaceNotFoundException;
import com.parking.lease.exception.ServiceException;
import com.parking.lease.exception.SpaceNotAvailableException;
import com.parking.lease.parkingSpace.ParkingSpace;
import com.parking.lease.parkingSpace.ParkingSpaceRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
@DisplayName("Testa a criação de uma locação de vaga")
public class RentalCreateServiceITest {

    @Mock
    private ParkingSpaceRepository parkingSpaceRepository;

    @InjectMocks
    private RentalCreateServiceImp rentalCreateService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    @DisplayName("Deve lançar exceção: vaga não encontrada")
    void shouldExceptionParkingSpaceNotFound() {

        when(parkingSpaceRepository.findById(anyLong())).thenReturn(Optional.empty());

        ServiceException exception = assertThrows(
                ParkingSpaceNotFoundException.class,
                () -> rentalCreateService.createRental(buildRentalDtoCreation()));

        assertEquals("Vaga não encontrada", exception.getMessage());
    }

    @Test
    @DisplayName("Deve lançar exceção: Vaga indisponível")
    void shouldExceptionSpaceNotAvailableException() {

        ParkingSpace parkingSpace = buildParkingSpace().get();
        parkingSpace.setAvailable(Boolean.FALSE);
        when(parkingSpaceRepository.findById(anyLong())).thenReturn(Optional.of(parkingSpace));

        ServiceException exception = assertThrows(
                SpaceNotAvailableException.class,
                () -> rentalCreateService.createRental(buildRentalDtoCreation()));

        assertEquals("Vaga indisponível", exception.getMessage());
    }

    private static RentalDTOCreation buildRentalDtoCreation() {
        return RentalDTOCreation.builder()
                .carLicensePlate("123abc")
                .spaceParkingId(1L)
                .build();
    }

    private static Optional<ParkingSpace> buildParkingSpace() {
        ParkingSpace parkingSpace = new ParkingSpace();
        parkingSpace.setId(1L);
        parkingSpace.setCarLicensePlate("123ABC");
        parkingSpace.setAvailable(Boolean.TRUE);
        parkingSpace.setPosition("A1");

        return Optional.of(parkingSpace);
    }

}
