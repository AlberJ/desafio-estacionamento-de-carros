package com.parking.lease.parkingSpace;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@Builder(builderMethodName = "builder")
public class ParkingSpaceDTOView {
    private Long id;
    private String position;
}
