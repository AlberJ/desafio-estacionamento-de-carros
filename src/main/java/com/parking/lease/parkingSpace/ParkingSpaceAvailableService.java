package com.parking.lease.parkingSpace;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
@FunctionalInterface
interface ParkingSpaceAvailableService {
    List<ParkingSpace> getListParkingSpaceAvailable();
}
