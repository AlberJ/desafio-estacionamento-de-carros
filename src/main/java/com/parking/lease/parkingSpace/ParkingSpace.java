package com.parking.lease.parkingSpace;

import com.parking.lease.rental.Rental;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ParkingSpace {

    @Id
    @Column(name = "parking_space_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String carLicensePlate;

    @NotBlank
    private String position;

    private boolean available = true;

    @OneToMany(mappedBy = "parkingSpace", fetch = FetchType.LAZY)
    private List<Rental> rents;
}
