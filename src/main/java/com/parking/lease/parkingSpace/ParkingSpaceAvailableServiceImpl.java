package com.parking.lease.parkingSpace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ParkingSpaceAvailableServiceImpl implements ParkingSpaceAvailableService{

    @Autowired
    private ParkingSpaceRepository repository;

    @Override
    public List<ParkingSpace> getListParkingSpaceAvailable() {
        return repository.findByAvailableTrue();
    }
}
