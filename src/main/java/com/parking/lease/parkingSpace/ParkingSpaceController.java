package com.parking.lease.parkingSpace;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/parkingspaces")
public class ParkingSpaceController {

    @Autowired
    private ParkingSpaceAvailableService service;

    @GetMapping
    public List<ParkingSpaceDTOView> getList() {
        List<ParkingSpace> parkingList = this.service.getListParkingSpaceAvailable();
        return parkingList.stream()
                .map(parkingSpace -> ParkingSpaceDTOView.builder()
                        .id(parkingSpace.getId())
                        .position(parkingSpace.getPosition())
                        .build()).collect(Collectors.toList());
    }

    @GetMapping(value = "/count")
    public int getNumberSpaceAvailable() {
        return this.service.getListParkingSpaceAvailable().size();
    }
}
