package com.parking.lease.rental;

import com.parking.lease.exception.ServiceException;
import org.springframework.stereotype.Service;

@Service
@FunctionalInterface
public interface RentalCreateService {
    void createRental(RentalDTOCreation rentalDTOCreation) throws ServiceException;
}
