package com.parking.lease.rental;

import com.parking.lease.exception.ParkingSpaceNotFoundException;
import com.parking.lease.exception.SaveException;
import com.parking.lease.exception.ServiceException;
import com.parking.lease.exception.SpaceNotAvailableException;
import com.parking.lease.parkingSpace.ParkingSpace;
import com.parking.lease.parkingSpace.ParkingSpaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class RentalCreateServiceImp implements RentalCreateService {

    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private ParkingSpaceRepository spaceRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public void createRental(RentalDTOCreation dtoCreation) throws ServiceException {
        Optional<ParkingSpace> optionalParkingSpace = spaceRepository.findById(dtoCreation.getSpaceParkingId());

        if (!optionalParkingSpace.isPresent()) throw new ParkingSpaceNotFoundException();
        ParkingSpace parkingSpace = optionalParkingSpace.get();

        if (!parkingSpace.isAvailable()) throw new SpaceNotAvailableException();

        Rental rental = Rental.builder()
                .parkingSpace(parkingSpace)
                .timeOfEntry(LocalDateTime.now())
                .build();

        try {
            rentalRepository.save(rental);

            parkingSpace.setAvailable(Boolean.FALSE);
            parkingSpace.setCarLicensePlate(dtoCreation.getCarLicensePlate());
            spaceRepository.save(parkingSpace);
        } catch (Exception e) {
            throw new SaveException();
        }
    }
}
