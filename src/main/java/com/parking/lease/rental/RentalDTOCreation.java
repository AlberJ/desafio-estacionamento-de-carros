package com.parking.lease.rental;

import lombok.Builder;
import lombok.Data;

@Builder(builderMethodName = "builder")
@Data
public class RentalDTOCreation {
    private Long spaceParkingId;
    private String carLicensePlate;
}
