package com.parking.lease.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ParkingSpaceNotFoundException extends ServiceException {

    public ParkingSpaceNotFoundException() {
        super("Vaga não encontrada");
    }
}
