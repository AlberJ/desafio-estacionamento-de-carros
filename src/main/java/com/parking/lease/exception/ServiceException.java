package com.parking.lease.exception;

public class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }
}
