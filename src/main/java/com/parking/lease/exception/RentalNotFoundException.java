package com.parking.lease.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class RentalNotFoundException extends ServiceException{
    public RentalNotFoundException() {
        super("Locação não encontrada");
    }
}
