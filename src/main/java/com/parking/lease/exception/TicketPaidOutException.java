package com.parking.lease.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class TicketPaidOutException extends ServiceException {
    public TicketPaidOutException() {
        super("Ticket já pago");
    }
}
