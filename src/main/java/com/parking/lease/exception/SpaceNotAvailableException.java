package com.parking.lease.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class SpaceNotAvailableException extends ServiceException {
    public SpaceNotAvailableException() {
        super("Vaga indisponível");
    }
}
