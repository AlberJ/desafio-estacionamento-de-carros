package com.parking.lease.exception;

public class SaveException extends ServiceException{
    public SaveException() {
        super("Erro ao salvar");
    }
}
