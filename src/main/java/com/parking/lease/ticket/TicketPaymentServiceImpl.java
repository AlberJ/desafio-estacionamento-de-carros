package com.parking.lease.ticket;

import com.parking.lease.exception.TicketNotFoundException;
import com.parking.lease.exception.TicketPaidOutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TicketPaymentServiceImpl implements TicketPaymentService {

    @Autowired
    private TicketRepository ticketRepository;

    @Override
    public Ticket paymentTicket(Long ticketId) throws TicketNotFoundException, TicketPaidOutException {
        Optional<Ticket> optionalTicket = ticketRepository.findById(ticketId);
        if (!optionalTicket.isPresent())
            throw new TicketNotFoundException();

        Ticket ticket = optionalTicket.get();

        if (ticket.getPaymentTime() != null)
            throw new TicketPaidOutException();

        ticket.setPaymentTime(LocalDateTime.now());

        return ticketRepository.save(ticket);
    }
}
