package com.parking.lease.ticket;

import com.parking.lease.exception.TicketNotFoundException;
import com.parking.lease.exception.TicketPaidOutException;
import org.springframework.stereotype.Service;

@Service
@FunctionalInterface
public interface TicketPaymentService {
    Ticket paymentTicket(Long ticketId) throws TicketNotFoundException, TicketPaidOutException;
}
