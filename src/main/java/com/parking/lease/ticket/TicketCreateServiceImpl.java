package com.parking.lease.ticket;

import com.parking.lease.exception.RentalNotFoundException;
import com.parking.lease.exception.SaveException;
import com.parking.lease.parkingSpace.ParkingSpace;
import com.parking.lease.parkingSpace.ParkingSpaceRepository;
import com.parking.lease.rental.Rental;
import com.parking.lease.rental.RentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class TicketCreateServiceImpl implements TicketCreateService {

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private RentalRepository rentalRepository;

    @Autowired
    private ParkingSpaceRepository parkingspaceRepository;

    private PriceCalculator priceCalculator = new PriceCalculator();

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Ticket createTicket(Long rentalId) throws RentalNotFoundException, SaveException {

        Optional<Rental> rentalOptional = rentalRepository.findById(rentalId);

        if (!rentalOptional.isPresent())
            throw new RentalNotFoundException();
        Rental rental = rentalOptional.get();

//        Encerra o aluguél definindo o tempo de saída
        if (rental.getExitTime() == null)
            rental.setExitTime(LocalDateTime.now());

        Double value = priceCalculator.calculatePrice(rental);
        ParkingSpace space = rental.getParkingSpace();

        Ticket ticket = Ticket.builder()
                .rental(rental)
                .carLicensePlate(space.getCarLicensePlate())
                .totalValue(value)
                .build();

        try {
            ticket = ticketRepository.save(ticket);

//            Libera a vaga após a criação do ticket
            space.setAvailable(Boolean.TRUE);
            space.setCarLicensePlate(null);
            parkingspaceRepository.save(space);
        } catch (Exception e) {
            throw new SaveException();
        }

        return ticket;
    }
}
