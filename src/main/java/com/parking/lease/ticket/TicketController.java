package com.parking.lease.ticket;

import com.parking.lease.exception.RentalNotFoundException;
import com.parking.lease.exception.SaveException;
import com.parking.lease.exception.TicketNotFoundException;
import com.parking.lease.exception.TicketPaidOutException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/tickets")
public class TicketController {

    @Autowired
    private TicketCreateService ticketCreateService;

    @Autowired
    private TicketPaymentService ticketPaymentService;

    @Autowired
    private IssueReportService reportService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED)
    public TicketDTOView createTicket(@RequestBody TicketDTOCreation ticketDto) throws RentalNotFoundException, SaveException {
        Ticket ticket = this.ticketCreateService.createTicket(ticketDto.getRentalId());

        return TicketDTOView.builder()
                .id(ticket.getId())
                .paymentTime(ticket.getPaymentTime())
                .carLicensePlate(ticket.getCarLicensePlate())
                .positionParkingSpace(ticket.getRental().getParkingSpace().getPosition())
                .totalValue(ticket.getTotalValue())
                .build();
    }

    @PutMapping(value = "/pay/{ticketId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TicketDTOView paymentTicket(@PathVariable Long ticketId) throws TicketPaidOutException, TicketNotFoundException {
        Ticket ticket = ticketPaymentService.paymentTicket(ticketId);

        return TicketDTOView.builder()
                .id(ticket.getId())
                .paymentTime(ticket.getPaymentTime())
                .carLicensePlate(ticket.getCarLicensePlate())
                .positionParkingSpace(ticket.getRental().getParkingSpace().getPosition())
                .totalValue(ticket.getTotalValue())
                .paymentTime(ticket.getPaymentTime())
                .build();
    }

    @GetMapping(value = "/report")
    public ReportDTO report() {
        return reportService.issueReport();
    }
}
