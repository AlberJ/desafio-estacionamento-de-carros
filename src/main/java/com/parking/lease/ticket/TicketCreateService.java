package com.parking.lease.ticket;

import com.parking.lease.exception.RentalNotFoundException;
import com.parking.lease.exception.SaveException;
import org.springframework.stereotype.Service;

@Service
@FunctionalInterface
public interface TicketCreateService {
    Ticket createTicket(Long rentalId) throws RentalNotFoundException, SaveException;
}
