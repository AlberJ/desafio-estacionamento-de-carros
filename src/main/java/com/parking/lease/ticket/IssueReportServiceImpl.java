package com.parking.lease.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IssueReportServiceImpl implements IssueReportService {

    @Autowired
    private TicketRepository repository;

    @Override
    public ReportDTO issueReport() {

        Double totalValues = repository.sumTicketsValues();

        Integer pendingTickets = repository.countPendingTickets();

        return ReportDTO.builder()
                .totalRentals(repository.count())
                .totalPayments(totalValues)
                .pendingTickets(pendingTickets)
                .build();
    }
}
