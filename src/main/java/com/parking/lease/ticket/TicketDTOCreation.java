package com.parking.lease.ticket;

import lombok.Data;

@Data
public class TicketDTOCreation {
    private Long rentalId;
}
