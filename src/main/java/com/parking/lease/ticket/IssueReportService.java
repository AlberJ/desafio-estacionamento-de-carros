package com.parking.lease.ticket;

import org.springframework.stereotype.Service;

@Service
@FunctionalInterface
public interface IssueReportService {
    ReportDTO issueReport ();
}
