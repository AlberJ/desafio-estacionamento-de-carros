package com.parking.lease.ticket;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query(value = "SELECT SUM(totalValue) FROM Ticket WHERE paymentTime IS NOT NULL")
    Double sumTicketsValues();

    @Query(value = "SELECT COUNT(id) FROM Ticket WHERE paymentTime IS NULL")
    Integer countPendingTickets();
}
