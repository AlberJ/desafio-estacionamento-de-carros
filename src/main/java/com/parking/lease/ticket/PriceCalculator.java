package com.parking.lease.ticket;

import com.parking.lease.rental.Rental;

public class PriceCalculator {
    private static Double value = 7.00;

    public Double calculatePrice(Rental rental) {
        Integer timeDifference = rental.getExitTime().compareTo(rental.getTimeOfEntry());

        if (timeDifference > 3) {
            timeDifference -= 3;
            value += timeDifference.doubleValue() * 3.0;
        }

        return value;
    }
}
