package com.parking.lease.ticket;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder(builderMethodName = "builder")
public class TicketDTOView {
    private Long id;
    private String positionParkingSpace;
    private String carLicensePlate;
    private Double totalValue;
    private LocalDateTime paymentTime;
}
