package com.parking.lease.ticket;

import lombok.Builder;
import lombok.Data;

@Data
@Builder(builderMethodName = "builder")
public class ReportDTO {
    Long totalRentals;
    Double totalPayments;
    Integer pendingTickets;
}
