# Desafio - Estacionamento de Carros 

O sistema para estacionamento de carros foi elaborado para gerenciar as vagas de maneira automática.  

Nele, o usuário do sistema, o proprietário do veiculo, verifica as vagas disponíveis, aluga uma e estaciona seu carro. Ao sair, gera o ticket e após confirmar os dados pode realizar o pagamento. Tudo feito pelo próprio usuário do estacionamento, substituindo os guichês de atendimento com funcionários por terminais de interface do sistema.

### Sobre as configurações e recursos utilizados

O sistema foi configurado para rodar na porta 8082, para alterar edite no arquivo `src/main/resources/application.properties` a propriedade `server.port` com o valor da porta desejada. O padrão é 8080.

O banco de dados utilizado é o H2 em memória, portanto, os dados são perdidos após encerrar a execução do sistema. O banco deve rodar atomaticamente sem precisar baixar dependência extra (além da contida no `pom.xml`) ou instalar algo. Suas configurações também encontram-se definidas no arquivo `src/main/resources/application.properties` após o comentário `#DB`.

As vagas de estacionamento são geradas automaticamente ao rodar o sistema e estão definidas no arquivo `src/main/resources/data.sql`.

### Para rodar o sistema

Import o sistema como projeto **Maven** e instale as dependências contidas no `pom.xml`. Após isso, basta executar o arquivo `src/main/java/com/parking/lease/ParkingApplication.java`.

### Para uso do sistema pelo Postman

Na raiz do sistema encontra-se a coleção de requisições `Desafio - Estacionamento de Carros.postman_collection.json`, basta importá-la no Postman.
 